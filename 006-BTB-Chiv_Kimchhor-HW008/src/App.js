import { Col, Container, Row } from "react-bootstrap";
import "./App.css";
import MyForm from "./components/MyForm";
import MyTable from "./components/MyTable";
import NavMenu from "./components/NavMenu";

import React, { Component } from "react";

export default class App extends Component {
  render() {
    return (
      <div>
        <NavMenu />

        <Container style={{ marginTop: 50 }}>
          <Row>
            <Col>
              <MyForm />
            </Col>
            <Col>
              <MyTable />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
