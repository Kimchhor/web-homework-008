import React, { Component } from "react";
import { Form, Button, Image } from "react-bootstrap";
export default class MyForm extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      emailErr: "",
      usernameErr: "",
      passwordErr: "",
      gender:"male"
    };
  }
  onHandleText = (e) => {
    
    console.log("event : " + e.target.name);
    //let name="username"
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        console.log(this.state);

        if (e.target.name === "username") {
          if (e.target.value === "") {
            this.setState({
              usernameErr: "Username can't be empty",
            });
          }
        }

        if (e.target.name === "email") {
          let pattern = /^\S+@\S+\.[a-z]{3}$/g;
          let result = pattern.test(this.state.email.trim());
          if (result) {
            this.setState({
              emailErr: "",
            });
          } else if (this.state.email === "") {
            this.setState({
              emailErr: "Email can't be empty",
            });
          } else {
            this.setState({
              emailErr: "Email is invalid",
            });
          }

          if (e.target.name === "password") {
            let pattern1 =
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[_\W])[a-zA-Z0-9_\W]{8,}$/g;
            let result1 = pattern1.test(this.state.password);
            if (result1) {
              this.setState({
                passwordErr: "",
              });
            } else if (this.state.password === "") {
              this.setState({
                passwordErr: "Password can't be empty",
              });
            } else {
              this.setState({
                passwordErr: "Password is invalid",
              });
            }
          }
        }
      }
    );
  };

  render() {
    return (
      <Form>
        <Image
          
          width="150px"
          height="150px"
          src="./img/profile.png"
          roundedCircle
        />
        <h3>Create Account</h3>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Username</Form.Label>
          <Form.Control
            onChange={this.onHandleText}
            name="username"
            type="email"
            placeholder="username"
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.usernameErr}</p>
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
        <Form.Label >Gender</Form.Label>
        <Form.Check 
        inline
        label="Male" 
        type={"radio"} 
        name="gender"
        defaultChecked={this.state.gender==="male"?true:false}
        />
        
        <Form.Check 
        inline label="Female" 
        type={"radio"} 
        name="gender" 
        defaultChecked={this.state.gender==="female"?true:false}
        />
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            onChange={this.onHandleText}
            name="email"
            type="email"
            placeholder="email"
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.emailErr}</p>
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            onChange={this.onHandleText}
            name="password"
            type="password"
            placeholder="password"
          />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.passwordErr}</p>
          </Form.Text>
        </Form.Group>

        <Button variant="primary" type="button">
          Save
        </Button>
      </Form>
    );
  }
}
