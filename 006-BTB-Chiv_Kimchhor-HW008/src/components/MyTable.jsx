import React, { Component } from "react";
import { Table } from "react-bootstrap";
export default class MyTable extends Component {
  render() {
    return (
      <div style={{marginTop:50}}>
          <h3>Table Accounts</h3>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Username</th>
              <th>Email</th>
              <th>Gender</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}
